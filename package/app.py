from flask import Flask
from flask_restx import Api
from config import Configuration
from models.Predictor import Predictor

app = Flask(__name__)
app.config.from_object(Configuration)
app.url_map.strict_slashes = False

api = Api(app, version='0.1', title='Password popularity API',
          description='Find out how often a password occurs in a million user passwords.',
          contact='greal-san@ya.ru',
          license='GPL',
          prefix='/api/',
          doc='/api/'
          )

predictor = Predictor(app.config['MODEL_PATH'])
