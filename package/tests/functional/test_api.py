import pytest

from app import app
from views.api import *


# python -m pytest tests/


@pytest.fixture(name='client')
def initialize_authorized_test_client():
    app.testing = True
    client = app.test_client()
    yield client
    app.testing = False


@pytest.mark.post
def test_predict_string(client):
    data = {
        "password": "string",
    }
    response = client.post('/api/inference/predict', json=data)
    assert response.status_code == 200
    response_json_data = response.get_json()
    assert response_json_data["popularity"] > 0


@pytest.mark.post
def test_predict_pass(client):
    data1 = {
        "password": "jgnfgjrd6urtshd",
    }
    response1 = client.post('/api/inference/predict', json=data1)
    assert response1.status_code == 200
    response_json_data1 = response1.get_json()

    data2 = {
        "password": "pass",
    }
    response2 = client.post('/api/inference/predict', json=data2)
    assert response2.status_code == 200
    response_json_data2 = response2.get_json()

    assert response_json_data2["popularity"] > response_json_data1["popularity"]
