from app import predictor


# python -m pytest tests/

def test_features_generation_abc123():
    password = "abc123"
    features = predictor._Predictor__generate_features(password)
    assert features == [0.0, 1.0, 1.0, 0.0, 1.0, 15.509775004326936, 6.0, 0.5, 0.5, 0.16666666666666666, 0.0, 0.0, 1.0]


def test_features_generation_hardpass():
    password = "hD3^#sJG1zG^"
    features = predictor._Predictor__generate_features(password)
    assert features == [0.0, 1.0, 0.0,
                        0.0, 0.0, 39.863137138648355,
                        12.0, 0.16666666666666666, 0.5833333333333334,
                        0.0, 0.3333333333333333, 0.0,
                        0.8333333333333334]
