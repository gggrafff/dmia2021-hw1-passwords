from flask_restx import Resource
from app import api, predictor
from models.ApiModels import password_model, prediction_model


ns = api.namespace('inference', description='Use the model')


@ns.route('/predict')
class Predict(Resource):
    """Accesses the model, calculates the prediction"""

    @ns.doc('Make a prediction')
    @ns.expect(password_model)
    @ns.marshal_with(prediction_model)
    def post(self):
        """Returns you how many times the password has been encountered in a million user passwords"""
        return {'popularity': predictor.predict(api.payload['password'])}
