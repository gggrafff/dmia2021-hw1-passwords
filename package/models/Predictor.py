import os
import re  # С помощью регулярок будем искать паттерны в строках (вхождение слов, дат и т.д.)
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
from catboost import CatBoostRegressor  # Просто запихаем всё в catboost
from password_strength import PasswordStats  # Либа для проверки качества паролей
from nltk.corpus import words  # Корпус слов для проверки того, что пароль содержит слово


def static_init(cls):
    """
    https://stackoverflow.com/a/60748729/12903113
    :param cls: Класс, который декорируется для инициализации статических полей.
    :return: Класс после инициализации статических полей.
    """
    if getattr(cls, "static_init", None):
        cls.static_init()
    return cls


@static_init
class Predictor:
    """
    Управление моделью.
    Произвести прогноз, переобучить.
    """
    ENGLISH_WORDS = None

    @classmethod
    def static_init(cls):
        """
        Инициализация статических полей класса.
        https://stackoverflow.com/a/60748729/12903113
        :return: None
        """
        if cls.ENGLISH_WORDS is None:
            try:
                cls.ENGLISH_WORDS = set(words.words())
            except Exception:
                import nltk
                nltk.download('words')
                cls.ENGLISH_WORDS = set(words.words())

    def __init__(self, path_to_model: str):
        """
        Конструктор.
        Если есть файл модели, то читает модель.
        Если нет файла модели - нужно обучить модель.
        :param path_to_model: Путь к файлу модели *.cbm
        """
        self.path_to_model = path_to_model
        if os.path.isfile(path_to_model):
            self.model = CatBoostRegressor()
            # https://catboost.ai/docs/concepts/python-reference_catboostregressor_load_model.html
            self.model.load_model(self.path_to_model)
        else:
            print("Create new model. Run training, please.")

    def predict(self, passwords):
        """
        Производит прогноз.
        Сколько раз встречается пароль в 1_000_000 реальных паролей.
        :param passwords: Один пароль в строке, список паролей или test dataframe.
        :return: Одно значение прогноза или массив значений.
        """
        if isinstance(passwords, list):
            features = [Predictor.__generate_features(password) for password in passwords]
        elif isinstance(passwords, str):
            features = Predictor.__generate_features(passwords)
        elif isinstance(passwords, pd.DataFrame):
            features = Predictor.__generate_dataframe_with_features(passwords)
        else:
            raise TypeError("Invalid type of 'passwords' argument.")

        # https://catboost.ai/docs/concepts/python-reference_catboostregressor_predict.html
        y_pred = self.model.predict(features)
        return np.exp(y_pred) - 1

    def fit(self, train_csv_url: str):
        df_train = pd.read_csv(train_csv_url)
        y_train = df_train['Times'].apply(lambda x: np.log(x + 1))
        x_train = self.__generate_dataframe_with_features(df_train)

        # https://catboost.ai/docs/concepts/python-reference_catboostregressor.html
        self.model = CatBoostRegressor(iterations=300,
                                       depth=11,
                                       l2_leaf_reg=20,
                                       learning_rate=0.5,
                                       loss_function='RMSE',
                                       logging_level='Silent')
        # https://catboost.ai/docs/concepts/python-reference_catboostregressor_fit.html
        self.model.fit(x_train, y_train)

        # https://catboost.ai/docs/concepts/python-reference_catboostregressor_save_model.html
        self.model.save_model(self.path_to_model,
                              format="cbm",
                              export_parameters=None,
                              pool=None)

    @staticmethod
    def __generate_features(password: str):
        """
        Функция генерит фичи для одного пароля
        """
        df = pd.DataFrame([password], columns=['Password'])
        Predictor.__generate_dataframe_with_features(df)
        return list(df.loc[0])

    @staticmethod
    def __generate_dataframe_with_features(df: pd.DataFrame):
        """
        Функция генерит фичи для всех паролей в датасете.
        Принимает в качестве параметра pandas.DataFrame.
        Работает inplace.
        """
        df['0_count'] = df['Password'].apply(lambda x: str(x).count('0'))  # количество нулей
        df['1_count'] = df['Password'].apply(lambda x: str(x).count('1'))  # количество единиц
        df['2_count'] = df['Password'].apply(lambda x: str(x).count('2'))  # количество двоек

        # Показатели либы password_strength
        df['repeated_patterns_length'] = df['Password'].apply(lambda x: PasswordStats(str(x)).repeated_patterns_length)
        df['weakness_factor'] = df['Password'].apply(lambda x: PasswordStats(str(x)).weakness_factor)
        df['entropy_bits'] = df['Password'].apply(lambda x: PasswordStats(str(x)).entropy_bits)

        df['char_count'] = df['Password'].apply(lambda x: len(str(x)))  # Длина пароля

        digit_count = df['Password'].apply(lambda x: len([x for x in str(x) if x.isdigit()]))
        df['digit_ratio'] = digit_count / df['char_count']  # Доля цифр

        isalpha_count = df['Password'].apply(lambda x: len([x for x in str(x) if x.isalpha()]))
        df['alpha_ratio'] = isalpha_count / df['char_count']  # Доля букв

        vowels = ['a', 'e', 'i', 'o', 'u']
        vowels_count = df['Password'].apply(lambda x: len([x for x in str(x).lower() if x in vowels]))
        df['vowels_ratio'] = vowels_count / df['char_count']  # Доля гласных

        upper_case = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                      'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
        upper_case_count = df['Password'].apply(lambda x: (len([x for x in str(x) if x in upper_case])))
        df['upper_case_ratio'] = upper_case_count / df['char_count']  # Доля заглавных букв

        # Проверим, не содержит ли пароль реального слова
        df['is_real_word'] = df['Password'].apply(
            lambda x: 1 if re.sub(r'[0-9]+', '', str(x).lower()) in Predictor.ENGLISH_WORDS else 0)

        unique_chars_count = df['Password'].apply(lambda x: len(set(str(x))))
        df['unique_chars_ratio'] = unique_chars_count / df['char_count']  # Доля уникальных символов

        if 'Password' in df.columns:
            del df['Password']
        if 'Times' in df.columns:
            del df['Times']

        return df
