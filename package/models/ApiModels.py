from flask_restx import fields
from app import api


password_model = api.model('Password', {
    'password': fields.String(required=True,
                              description='The password to check for popularity')
})

prediction_model = api.model('Prediction', {
    'popularity': fields.Float(required=True,
                               description='How many times the password has been encountered in a million user '
                                           'passwords')
})
