import os


class Configuration(object):
    DEBUG = False
    MODEL_PATH = os.path.dirname(os.path.abspath(__file__)) + "/resources/model.cbm"
